/**
 * Created by Zowie on 24-09-16.
 */

var menuManager = {
    init: function () {
        menuManager.setListeners();
    },
    setListeners: function () {
        $('.hamburger, .cross').on('click', function () {
            menuManager.functions.toggleMenu();
        });
    },
    functions: {
        toggleMenu: function () {
            $('.menu').toggle();
            $('.cross').toggle();
            $('.hamburger').toggle();
        }
    }
};

var videoManager = {
    init: function () {
        videoManager.setListeners();
    },
    setListeners: function () {
        $('.btn-video').on('click', function () {
            videoManager.functions.fullscreen();
            videoManager.functions.videoApp();
            videoManager.functions.playVideo();
        });

        $('.move-down').on('click', function () {
            videoManager.functions.skipVideo();
        });

        $(window).scroll(function(){
            if ($(this).scrollTop() > 50) {
                videoManager.functions.videoAppMuted();
            }
        });

    },
    functions: {
        fullscreen: function () {
            var elem = document.getElementById("bgvid");
            if (elem.requestFullscreen) {
                elem.requestFullscreen();
            } else if (elem.mozRequestFullScreen) {
                elem.mozRequestFullScreen();
            } else if (elem.webkitRequestFullscreen) {
                elem.webkitRequestFullscreen();
            }

        },

        setTimeout: function () {
            $('#header').addClass('black-opacity');
            $('#bgvid').addClass('vid-index');
            $('.menu-wrapper').show();
            $('.video-content').show();
        },

        videoAppMuted: function () {
            $("#bgvid").prop("controls", false);
            $("#bgvid").prop("muted", true);
        },

        videoApp: function () {
            $("#bgvid").prop("controls", true);
            $("#bgvid").prop("muted", false);
        },

        playVideo: function () {
            document.getElementById('bgvid').currentTime = 0;
            document.getElementById('bgvid').play();
        },

        skipVideo: function () {
            scrollManager.functions.autoScrollElement('#verkiezingen', 1000);
        },

        runFunction: function () {
            var elem = document.getElementById("bgvid");
            if (elem.currentTime > 75) {

                if (document.exitFullscreen) {
                    document.exitFullscreen();
                } else if (document.msExitFullscreen) {
                    document.msExitFullscreen();
                } else if (document.mozCancelFullScreen) {
                    document.mozCancelFullScreen();
                } else if (document.webkitExitFullscreen) {
                    document.webkitExitFullscreen();
                }

                $("#bgvid").prop("controls", false);
                $("#bgvid").prop("muted", true);
                videoManager.functions.setTimeout();
            }
        }
    }
};

var t = setInterval(videoManager.functions.runFunction, 500);

var countManager = {
    init: function () {
        var options = {
            useEasing: true,
            useGrouping: true
        };

        var kiesmannenCount = new CountUp("kiesmannenCount", 0, 538, 0, 4.5, options);
        var republikeinenCount = new CountUp("republikeinenCount", 0, 279, 0, 4.5, options);
        var democratenCount = new CountUp("democratenCount", 0, 228, 0, 4.5, options);

        var queryMobileCount = Modernizr.mq('(max-width: 767px)');
        var queryTabletCount = Modernizr.mq('(max-width: 991px)');

        if (queryMobileCount) {
            var waypointCountUpKiesmannenMobile = new Waypoint({
                element: document.getElementById('totaal-kiesmannen'),
                handler: function() {
                    kiesmannenCount.start();
                },
                offset: 550
            });

            var waypointCountUpRepublikeinenMobile = new Waypoint({
                element: document.getElementById('totaal-republikeinen'),
                handler: function() {
                    republikeinenCount.start();
                },
                offset: 650
            });

            var waypointCountUpDemocratenMobile = new Waypoint({
                element: document.getElementById('totaal-democraten'),
                handler: function() {
                    democratenCount.start();
                },
                offset: 750
            });
        } else if(queryTabletCount) {
            var waypointCountUpKiesmannenTablet = new Waypoint({
                element: document.getElementById('totaal-kiesmannen'),
                handler: function() {
                    kiesmannenCount.start();
                },
                offset: 750
            });

            var waypointCountUpRepublikeinenTablet = new Waypoint({
                element: document.getElementById('totaal-republikeinen'),
                handler: function() {
                    republikeinenCount.start();
                },
                offset: 850
            });

            var waypointCountUpDemocratenTablet = new Waypoint({
                element: document.getElementById('totaal-democraten'),
                handler: function() {
                    democratenCount.start();
                },
                offset: 950
            });
        } else {
            var waypointCountUpKiesmannen = new Waypoint({
                element: document.getElementById('totaal-kiesmannen'),
                handler: function() {
                    kiesmannenCount.start();
                },
                offset: 850
            });

            var waypointCountUpRepublikeinen = new Waypoint({
                element: document.getElementById('totaal-republikeinen'),
                handler: function() {
                    republikeinenCount.start();
                },
                offset: 950
            });

            var waypointCountUpDemocraten = new Waypoint({
                element: document.getElementById('totaal-democraten'),
                handler: function() {
                    democratenCount.start();
                },
                offset: 1050
            });
        }

        $(document).scroll(function() {
            if ($(window).scrollTop() === 0) {
                kiesmannenCount.reset();
                republikeinenCount.reset();
                democratenCount.reset();
            }
        });
    }
};

var votes = [];

var hoverManager = {
    init: function () {
        hoverManager.functions.hoverOverlay();
        hoverManager.functions.removeGesture();
        hoverManager.setListeners();
    },
    setListeners: function () {
        $('.vote-yes').on('click', function () {
            hoverManager.functions.voteYes($(this));
        });

        $('.vote-no').on('click', function () {
            hoverManager.functions.voteNo($(this));
        });
    },
    functions: {
        hoverOverlay: function () {
            if (Modernizr.touchevents) {
                // handle the adding of hover class when clicked
                $(".img").click(function (e) {
                    if (!$(this).hasClass("hover")) {
                        $(this).addClass("hover");
                    }
                });

            } else {
                $(".img").hover(
                    function() {
                        $( this ).addClass( "hover" );
                    }, function() {
                        $( this ).removeClass( "hover" );
                    }
                );
            }
        },

        voteYes : function ($voteYesButton) {
            $voteYesButton.closest('.img').find('.overlay-yes').addClass('display').removeClass('display-hidden');
            $voteYesButton.closest('.img').find('.big-yes').addClass('fadeIn');
            $voteYesButton.closest('.img').find('.overlay').remove();

            votes.push($voteYesButton.attr('data-president'));

            hoverManager.functions.result();
        },

        voteNo : function ($voteNoButton) {
            $voteNoButton.closest('.img').find('.overlay-no').addClass('display').removeClass('display-hidden');
            $voteNoButton.closest('.img').find('.big-no').addClass('fadeIn');
            $voteNoButton.closest('.img').find('.overlay').remove();

            votes.push($voteNoButton.attr('data-president'));

            hoverManager.functions.result();
        },

        result : function () {

            if(votes.length == 12) {

                $(".stemhelper-result-toggle").removeClass("stemhelper-result").fadeIn("slow");

                var total = 12;
                var counts = {};

                votes.forEach(function(x) { counts[x] = (counts[x] || 0)+1; });

                var trump = parseInt(counts.trump);
                var clinton = parseInt(counts.clinton);

                var percTrump = Math.round((trump / total) * 100);
                var percClinton = Math.round((clinton / total) * 100);

                if(clinton > trump) {
                    $( ".trump-helper-percentage" ).append('<span class="text-white">' + percTrump + '%</span>');
                    $( ".clinton-helper-percentage" ).append('<span class="text-green">' + percClinton + '%</span>');
                } else if (clinton < trump) {
                    $( ".trump-helper-percentage" ).append('<span class="text-green">' + percTrump + '%</span>');
                    $( ".clinton-helper-percentage" ).append('<span class="text-white">' + percClinton + '%</span>');
                } else {
                    $( ".trump-helper-percentage" ).append('<span class="text-green">' + percTrump + '%</span>');
                    $( ".clinton-helper-percentage" ).append('<span class="text-green">' + percClinton + '%</span>');
                }

                $('#stem-result').velocity("scroll", {
                    duration: 1000,
                    easing: "easeInBack",
                    offset: "-140"
                });
            }
        },

        removeGesture: function () {
                $('.gesture-remover').on('click', function () {
                    $('.gestures').hide().fadeOut('slow');
                })
        }
    }
};

var svgClicker = {
    init: function () {
        svgClicker.setListeners();
    },

    setListeners: function () {
        $('.mapusa-usa').on('click', function () {
            svgClicker.functions.clickSvg();
        })
    },

    functions: {
        clickSvg: function () {
            $("#Layer_2").find("g").each(function (i) {
                $(this).addClass("hideBalloon").fadeOut();
            });

            var country = "#" + event.target.id + "Text";
            var countryMobile = "#" + event.target.id + "TextMobile";
            var query = Modernizr.mq('(min-width: 480px)');

            if (query) {
                $(country).fadeIn().removeClass("hideBalloon");
            }

            else {
                $(countryMobile).fadeIn().removeClass("hideBalloon");
            }

        }
    }
};

var scrollManager = {
    init: function () {
        scrollManager.setListeners();
    },

    setListeners: function () {
        scrollManager.functions.scrollToElement('a#homeMenu', '#top', 1000);
        scrollManager.functions.scrollToElement('a#verkiezingenMenu', '#verkiezingen', 1000);
        scrollManager.functions.scrollToElement('a#kiesmannenMenu', '#kiesman', 1000);
        scrollManager.functions.scrollToElement('a#stemhelperMenu', '#stemhelper', 1000);
        scrollManager.functions.scrollToElement('a#stemmenMenu', '#stemmen', 1000);
        scrollManager.functions.scrollToElement('a#informatieMenu', '#informatie', 1000);
        scrollManager.functions.scrollToElement('a#homeMenu2', '#top', 1000);
        scrollManager.functions.scrollToElement('a#verkiezingenMenu2', '#verkiezingen', 1000);
        scrollManager.functions.scrollToElement('a#kiesmannenMenu2', '#kiesman', 1000);
        scrollManager.functions.scrollToElement('a#stemhelperMenu2', '#stemhelper', 1000);
        scrollManager.functions.scrollToElement('a#stemmenMenu2', '#stemmen', 1000);
        scrollManager.functions.scrollToElement('a#informatieMenu2', '#informatie', 1000);
    },

    functions: {
        scrollToElement: function ($element, $scrollId, $duration) {
            $($element).on('click', function () {
               scrollManager.functions.autoScrollElement($scrollId, $duration);
            });
        },

        autoScrollElement: function ($scrollId, $duration) {

            var queryMobile = Modernizr.mq('(max-width: 767px)');
            var queryTablet = Modernizr.mq('(max-width: 991px)');

            if (queryMobile) {
                $($scrollId).velocity("scroll", {
                    duration: $duration,
                    easing: "easeInBack"
                });
            } else if(queryTablet) {
                $($scrollId).velocity("scroll", {
                    duration: $duration,
                    easing: "easeInBack",
                    offset: "-150"
                });
            } else {
                $($scrollId).velocity("scroll", {
                    duration: $duration,
                    easing: "easeInBack",
                    offset: "-100"
                });
            }
        }
    }
};

var voteManager = {
    init: function () {
        voteManager.setListeners();
    },

    setListeners: function () {
        $('.voteButton').click(function() {
            voteManager.functions.makeVote(
                //Looks if children elements contain string "trump"
                $(this).html().indexOf('trump'));
        });

        $('.btn-vote').on('click', function () {
            $('#stemcontainer').velocity("scroll", {
                duration: 1000,
                easing: "easeInBack",
                offset: "-150"
            });
        });
    },

    functions: {
        makeVote: function (vote) {
            var trumpCount = 25; //moet reader worden
            var clintonCount = 25; //moet reader worden

            //if string contained trump returned value (vote) will be 0 or higher. otherwise return value = -1
            if (vote != -1) {
                trumpCount++;
            } else {
                clintonCount++;
            }

            var totaalCount = trumpCount + clintonCount;
            var percentageTrump = (trumpCount / totaalCount) * 100;
            var myBarLeft = document.getElementById('myBarLeft');
            var myProgressLeft = document.getElementById('myProgressLeft');
            var textPercentageTrump;
            var textPercentageClinton;
            var percentageClinton = (clintonCount / totaalCount) * 100;
            var myBarRight = document.getElementById('myBarRight');
            var myProgressRight = document.getElementById('myProgressRight');

            $('#stem-resultaten').removeClass('hide');
            $('.voteButton').addClass('hide');

            myProgressLeft.style.width = percentageTrump + '%';
            myProgressRight.style.width = percentageClinton + '%';
            textPercentageTrump = parseFloat(percentageTrump).toFixed(0);
            textPercentageClinton = parseFloat(percentageClinton).toFixed(0);

            document.getElementById('labelLeft').innerHTML = textPercentageTrump + '%';
            document.getElementById('labelRight').innerHTML = textPercentageClinton + '%';


        }
    }
};

var scrollManagerTop = {
    init: function () {

        var queryTabletScroll = Modernizr.mq('(max-width: 991px)');

        if(queryTabletScroll) {

            $(window).scroll(function(){
                if ($(this).scrollTop() > 100) {
                    $('#back-to-top').fadeIn();
                } else {
                    $('#back-to-top').fadeOut();
                }
            });

            //Click event to scroll to top
            $('#back-to-top').click(function(){
                $('html, body').animate({scrollTop : 0},1000);
                return false;
            });
        } else {
            $('#back-to-top').hide();
        }
    }
};




$(document).ready(function () {
    // importing AOS
    AOS.init();

    // loading progressbar
    NProgress.configure({ easing: 'ease', speed: 1500 });
    NProgress.start();

    // loading managers
    menuManager.init();
    scrollManager.init();
    videoManager.init();
    svgClicker.init();
    countManager.init();
    hoverManager.init();
    voteManager.init();
    scrollManagerTop.init();

    // progress is done
    NProgress.done();

});
