# IPMEDT2 - Groep J

In opdracht van school moesten wij voor de NOS een interactieve website maken uit één van de volgende onderwerpen:
- In Molenbeek
- Het succes van Max Verstappen
- De vluchtelingencrisis
- De Amerikaanse verkiezingen

Wij hebben gekozen voor de Amerikaanse verkiezingen omdat dit ons erg aansprak.

### Library's

De Library's die we gebruikt hebben voor het project zijn:

* [Gulp] - the streaming build system
* [BrowserSync] - Syncing browers en remote debugging
* [Velocity] - jQuery animate lib
* [jQuery] - jQuery
* [AOS] - Animate on scroll lib
* [Modernizr] - Modernizr
* [CountUp.js] - Countup js
* [nProgress] - Progress bar
* [wayPoints] - Waypoints js


### Installatie

Clone de laatste repo op [IPMEDT2J](https://zowie93@bitbucket.org/ipmedt2j/ipmedt2-j.git)

1.Clone de repo.

```sh
$ git clone https://zowie93@bitbucket.org/ipmedt2j/ipmedt2-j.git
```

2.Installeer Gulp
```sh
$ npm install --global gulp-cli
```

3.Installeer de node modules via NPM

```sh
$ npm install
```

4.Installeer packages via bower

```sh
$ bower install
```

5.Kopieer de assets met gulp

```sh
$ gulp copy-assets
```

6.Draai de gulp watcher in combinatie met [BrowserSync]
```sh
$ gulp watch-bs
```

BrowserSync opent automatisch de standaard browser met [localhost](http://127.0.0.1:3000)


### Todos

 - Unit testing (mocha)
 - A/B testing
 - ReThinkDB implementatie



License
----

MIT


**Zowie van Geest |**
**Gerson Straver |**
**Rik Wolsheimer |**
**Ben van Eekelen**


   [jQuery]: <http://jquery.com>
   [Gulp]: <http://gulpjs.com>
   [Velocity]: <https://github.com/julianshapiro/velocity>
   [BrowserSync]: <https://www.browsersync.io/>
   [AOS]: <https://michalsnik.github.io/aos/>
   [Modernizr]: <https://modernizr.com/>
   [CountUp.js]: <https://inorganik.github.io/countUp.js/>
   [nProgress]: <http://ricostacruz.com/nprogress/>
   [wayPoints]: <http://imakewebthings.com/waypoints/>
